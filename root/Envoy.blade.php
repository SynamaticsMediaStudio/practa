@servers(['localhost' => '127.0.0.1'])
@story('deploy')
    git
    composer
    php
@endstory

@task('init')
    cp .env.example .env && 
    php artisan key:generate
@endtask
