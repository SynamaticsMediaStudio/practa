<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function Mis()
    {
        return $this->hasMany('App\DailyMIS','product','code');
    }
}
