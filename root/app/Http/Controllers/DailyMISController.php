<?php

namespace App\Http\Controllers;

use App\DailyMIS;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Csv\Reader;

class DailyMISController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('daily-mis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'upload'=>'file|required',
            'date'=>'required|date',
        ];
        $messages = [
            'upload.required'=>'Please Upload a File',
            'date.required'=>'Please Select a date'
        ];
        $request->validate($rules,$messages);
        $user = Auth()->id();

        if($request->hasfile('upload')){
            $csv = Reader::createFromPath($request->file('upload'), 'r');
            $csv->setHeaderOffset(0);
            
            $uploaded = 0;
            $created = 0;
            $updated = 0;
            $items = [];
            foreach ($csv as $key) {
                $items[] = array_change_key_case($key);
            }
            foreach ($items as $key) {
                $code = $key['code'];
                $stock = $key['stock'];
                $mis = DailyMIS::where(['mis_date'=>Carbon::today(),'product'=>$code,'user'=>$user])->first();
                if($mis){
                    $updated++;
                }
                else{
                    $mis = new DailyMIS;
                    $created++;
                }
                $uploaded++;

                // Update
                $mis->product = $code;
                $mis->stock = $stock;
                $mis->user = $user;
                $mis->mis_date = $request->date;
                $mis->save();
            }
            return redirect()->back()->with(['success'=>"MIS Updated. Uploaded $uploaded, Created $created and Updated $updated MIS."]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailyMIS  $dailyMIS
     * @return \Illuminate\Http\Response
     */
    public function show(DailyMIS $dailyMIS)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DailyMIS  $dailyMIS
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyMIS $dailyMIS)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailyMIS  $dailyMIS
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyMIS $dailyMIS)
    {
        $date = Carbon::parse($request->date);
        $user = auth()->user()->id;
        if($request->hasfile('upload')){
            $csv = Reader::createFromPath($request->file('upload'), 'r');
            $csv->setHeaderOffset(0);
        }
        else{
            exit;
        }
        $updated    = 0;
        $uploaded   = 0;
        foreach ($csv as $key) {
            $uploaded++;
            $item = array_change_key_case($key);
            $mis  = DailyMIS::where(['mis_date'=>$date,'product'=>$item['reduce code'],'user'=>$user])->first();
            if($mis){
                $mis->stock = $mis->stock - $item['reduce stock'];
                $mis->save();
                $updated++;
            }
        }
        return redirect()->back()->with(['success'=>"MIS Updated. Uploaded $uploaded, Updated $updated MIS."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailyMIS  $dailyMIS
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyMIS $dailyMIS)
    {
        //
    }

    /**
     * MIS Check
     *
     * @param  \App\DailyMIS  $dailyMIS
     * @return \Illuminate\Http\Response
     */
    public static function misCheck()
    {
        $user = Auth::user()->id;
        $mis = DailyMIS::where(['mis_date'=>Carbon::today(),'user'=>$user])->get();
        if($mis->count()){
            return true;
        }
        else{
            return false;
        }
    }
}
