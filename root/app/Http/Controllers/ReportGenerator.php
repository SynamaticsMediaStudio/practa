<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Reader;
use App\Product;
use App\DailyMIS;
use Carbon\Carbon;
use League\Csv\Writer;
use SplTempFileObject;
class ReportGenerator extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('generator');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        $user   = $request->user()->id;
        if($request->hasfile('sales') && $request->hasfile('amazon_stock')){
            $sales          = Reader::createFromPath($request->file('sales'), 'r');
            $amazon_stock   = Reader::createFromPath($request->file('amazon_stock'), 'r');
            // Setting Delimiter for Version 2.0 
            if($request->file('amazon_stock')->getClientOriginalExtension() == 'txt'){
                $amazon_stock->setDelimiter("\t");
            }
            if($request->file('sales')->getClientOriginalExtension() == 'txt'){
                $sales->setDelimiter("\t");
            }
            $sales->setHeaderOffset(0);
            $amazon_stock->setHeaderOffset(0);
            $data   =   [];
            $amazon = [];

            foreach ($amazon_stock as $stock) {
                $stock          = array_change_key_case($stock);
                $sku            = (isset($stock['seller-sku'])) ?$stock['seller-sku']:$stock['sku'];
                $product        = Product::where(['SKU'=>$sku,'user'=>$user])->first();
                if($stock['warehouse-condition-code'] == "SELLABLE"){
                    $amazon[$sku]['quantity']   = $stock['quantity available'];
                }
            }
            foreach($sales as $sale){
                $sale           = array_change_key_case($sale);
                $sku            = $sale['sku'];
                $product        = Product::where(['SKU'=>$sale['sku'],'user'=>$user])->first();
                $data[]         = 
                                ['quantity'=>$sale['quantity'],
                                'sku'=>$sku,
                                'product'=> ($product) ? true:false,
                                'order-status'=>$sale['order-status']];
            }
            $order_status = array_unique(array_column($data, 'order-status'));
            return view('select-options',['data'=>$data,'sortItems'=>$order_status,'amazon'=>$amazon]);
        }
    }
    public function process(Request $request)
    {
        $user   = auth()->user()->id;
        $data   = json_decode($request->data);
        $amazon = json_decode($request->amazon);
        $status = $request->sorter;

        $collecter  = [];
        $final      = [];

        foreach ($data as $key) {
            // Create Product 
            $product = Product::where(['SKU'=>$key->sku,'user'=>$user])->first();
            if(!$product && isset($request->code[$key->sku])){
                $product = new Product;
                $product->SKU   = $key->sku;
                $product->code  = $request->code[$key->sku];
                $product->user  = $user;
                $product->save();
            }         
            $mis    = DailyMIS::where(['mis_date'=>Carbon::today(),'product'=>$product->code,'user'=>$user])->first();
            
            $isl = (empty($status)) ? []:$status;
            if(in_array($key->{'order-status'}, $isl)){
                $var    = $key->quantity;
                $sku    = $key->sku;
                $sales = isset($collecter[$key->sku]) ? $var+$collecter[$key->sku]['sales']:$var; 
                $collecter[$key->sku] = 
                        [
                            'sku'=>$key->sku,
                            'sales'=>$sales,
                            'stock'=>isset($amazon->{$sku})?$amazon->{$sku}->quantity:0
                        ];
                $collecter[$key->sku]['balance']    = $collecter[$key->sku]['stock']-$collecter[$key->sku]['sales'];
                $collecter[$key->sku]['master']     = ($mis) ? $mis->stock:"Stock for Today not Updated";
                $collecter[$key->sku]['code']       = $product->code;
            }
        }
        $file= new SplTempFileObject();
        $csv = Writer::createFromFileObject($file);
        $csv->insertOne(['SKU', 'QTY','MASTER QTY','CODE','STATUS']);         
        foreach ($collecter as $key) {
            if ($key['master'] < $key['balance'] ||$key['balance'] < 0 || $key['master'] == "Stock for Today not Updated"){
                $csv->insertOne([$key['sku'], $key['balance'],$key['master'],$key['code'],'SHORT']);
            }
            else{
                $csv->insertOne([$key['sku'], $key['balance'],$key['master'],$key['code'],'']);
            }
        }
        $csv->output(Carbon::today()->toDateString().'.csv');
        die;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
