<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// use App\DailyMIS;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the Generator.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function generate()
    {
        return view('generator');
    }
    /**
     * Show the Generator.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function UpdateInventory()
    {
        if(DailyMISController::misCheck()){
            return view('update-inventory');
        }
        else{
            return redirect()->route('inventory.create');
        }
    }
}
