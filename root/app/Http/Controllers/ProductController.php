<?php

namespace App\Http\Controllers;

use Auth;
use App\Product;
use Illuminate\Http\Request;
use League\Csv\Reader;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Arrays and Qs 
        $paginatables = [5,10,20,30,40,50];
        $page = $paginatables[0];
        $query = ['user'=>$request->user()->id];

        // Filters
        if($request->counts){
            $page = $request->counts;
        }
        if($request->sortby){
            
        }
        if($request->search){
            $query[] = ['SKU','LIKE',"%$request->search%"];
        }
        
        $products = Product::where($query)->paginate($page);

        // return $products;
        return view('products.index',['products'=>$products,'request'=>$request,'paginatables'=>$paginatables]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'SKU'=>'unique:products|required_without_all:upload',
            'code'=>'required_without_all:upload',
            'upload'=>'required_without_all:SKU,code',
        ];
        $messages = [
            'upload.required_without_all'=>"We need a valid File",
            'SKU.required_without_all'=>'We Need an SKU!',
            'code.required_without_all'=>'We Need an Product Code'
        ];
        $request->validate($rules,$messages);

        if($request->hasfile('upload')){
            $csv = Reader::createFromPath($request->file('upload'), 'r');
            $csv->setHeaderOffset(0);
            $created = 0;
            $uploaded = 0;
            foreach ($csv as $key) {
                $uploaded++;
                $sku    = (isset($key['SKU'])) ? $key['SKU']:$key['sku'];
                $code    = (isset($key['CODE'])) ? $key['CODE']:$key['code'];
                $product = Product::where(['user'=>$request->user()->id,'SKU'=>$sku])->first();
                if(!$product){
                    $product = new Product;
                    $product->SKU   = $sku;
                    $product->code  = $code;
                    $product->user  = Auth::user()->id;
                    $product->save();
                    $created++;
                }
            }
            return redirect()->back()->with(['success'=>"Created $created products from $uploaded uploaded products"]);
        }
        else{
            $product = Product::where(['user'=>$request->user()->id,'SKU'=>$request->SKU])->first();
            if($product){

            }
            else{
                $product = new Product;
            }
            $product->SKU   = $request->SKU;
            $product->code  = $request->code;
            $product->user  = Auth::user()->id;
            $product->save();
            return redirect()->back()->with(['success'=>"Created new Product $request->sku"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.product',['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product,Request $request)
    {
        $product->delete();
        return redirect()->back()->with(['success'=>'Deleted Product']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function deleteall(Request $request)
    {
        $ps = Product::all();
        foreach ($ps as $key) {
            $key->delete();
        }
        return redirect()->back()->with(['success'=>'All Products Deleted']);
    }
}
