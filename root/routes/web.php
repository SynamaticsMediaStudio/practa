<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');
Route::resource('/products', 'ProductController');
Route::delete('/deleteall', 'ProductController@deleteall');
Route::resource('/inventory', 'DailyMISController');
Route::resource('/generate', 'ReportGenerator');
Route::post('/process/generate/', 'ReportGenerator@process')->name('process-generator');
Route::resource('/settings', 'SettingController');
Route::get('/update-inventory', 'HomeController@UpdateInventory')->name('update-inventory');
Route::resource('/account', 'UserController');

Auth::routes();

