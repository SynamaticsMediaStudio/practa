@extends('layouts.app')
@section('title',"Data Generator")
@section('content')
    <div class="container">
        <div class="p-2 bg-danger text-white">
            <strong>
                The system is unable to find a product with the SKU's : 
                @foreach ($data as $item) @if ($item->code == "Product not Available"){{$item->sku}} @endif @endforeach                
            </strong>
            <br/>
            To continue, please enter the Code and Master Quantity to create product and generate the reports
        </div>
        <form action="{{route('process-generator')}}" method="post" class="form">
            @csrf
            <textarea type="hidden" name="data" class="d-none">
                    {{json_encode($data)}}
            </textarea>
            <table class="table table-sm">
                <thead class="table-dark">
                    <tr>
                        <th>SKU</th>
                        <th>QTY</th>
                        <th>CODE</th>
                        <th>MASTER QTY</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data as $item)
                    @if ($item->code == "Product not Available")
                    <tr class="bg-danger text-white">
                        <td>{{$item->sku}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>
                            <div class="form-group">
                                <input type="text" required name="code[{{$item->sku}}]" value="{{old('code'.$item->sku)}}" id="code_{{$item->sku}}" class="form-control form-control-sm">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text"  name="master[{{$item->sku}}]" value="{{old('master'.$item->sku)}}" id="master_{{$item->sku}}" class="form-control form-control-sm">
                            </div>
                        </td>
                    </tr>
                    @else
                    <tr>
                        <td>{{$item->sku}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{$item->code}}</td>
                        <td>{{$item->master}}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
            <button class="btn btn-success" type="submit">Save</button>
        </form>
    </div>
@endsection