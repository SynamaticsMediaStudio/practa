@extends('layouts.app')
@section('title',"Create Product")
@section('content')

<div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10">
            <h5 class="title">Report Generator {{Carbon::today()->toFormattedDateString()}}</h5>
            </div>
            <div class="col-sm-12 col-md-2 text-right">
                
            </div>
        </div>
    <hr>  
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <form action="{{route('generate.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="sales">Sales CSV</label>
                    <input type="file" name="sales" id="sales">
                    <a href="/sales.csv" class="text-danger" download>Download Sample File</a>
                </div>
                <div class="py-2"></div>
                <div class="form-group">
                    <label for="amazon_stock">Amazon Stock CSV</label>
                    <input type="file" name="amazon_stock" id="amazon_stock">
                    <a href="/fba.csv" class="text-danger" download>Download Sample File</a>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Generate Report</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
