@extends('layouts.app')
@section('title',"Create Product")
@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10">
            <h1 class="">Create Product</h1>
        </div>
        <div class="col-sm-12 col-md-2 text-right">
            <a href="{{route('products.index')}}" class="btn btn-outline-secondary">Back </a>
        </div>
    </div>
    <hr>  
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="SKU">SKU</label>
                    <input type="text" name="SKU" id="SKU" class="form-control">
                </div>
                <div class="form-group">
                    <label for="code">Product Code</label>
                    <input type="text" name="code" id="code" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm">Save</button>
                </div>
            </form>
        </div>
        <div class="col-sm-12 col-md-4 offset-md-2">
            <a href="/products.csv" class="text-danger" download>Download Sample File</a>
            <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="upload">Upload CSV/TXT</label>
                    <input type="file" name="upload" id="upload" class="" accept=".txt,.csv">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>  
</div>
@endsection
