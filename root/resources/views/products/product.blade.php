@extends('layouts.app')
@section('title',"Products")
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <h1 class="">{{$product->SKU}}</h1>
        </div>
        <div class="col-sm-12 col-md-4 offset-md-2 text-right">

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Updated On</th>
                        <th>Stock</th>
                        <th>Last Updated </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product->Mis()->where('user',Auth::user()->id)->orderBy('mis_date','asc')->get() as $mis)
                        <tr>
                            <th>{{Carbon::parse($mis->mis_date)->toFormattedDateString()}}</th>
                            <th>{{$mis->stock}}</th>
                            <th>{{Carbon::parse($mis->updated_at)->diffForHumans()}}</th>
                        </tr>
                    @endforeach
                    @empty($product->Mis)
                        <tr>
                            <th colspan="2" class="text-center">No Records Updated.</th>
                        </tr>
                    @endempty
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
