@extends('layouts.app')
@section('title',"Products")
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <h1 class="">Products</h1>
            <strong><i>{{$products->total()}} Total</i></strong>
        </div>
        <div class="col-sm-12 col-md-4 offset-md-2 text-right">
            <form action="/deleteall" method="POST" class="form form-sm" id="deleteForm" onsubmit="ProcessForm()">
                @csrf
                <a href="{{route('products.create')}}" class="btn btn-outline-primary">Create / Upload Product </a>
                <input type="hidden" name="_method" value="DELETE">
                <button class="btn btn-outline-danger " type="submit">Delete All Products</button>
            </form>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12 col-md-2 border-right">
            <form action="" class="form form-sm">
                <div class="form-group">
                    <label for="search">Search</label>
                    <input type="search" name="search" id="search" autocomplete="off" class="form-control" value="{{ $request->search }}">
                </div>
                <div class="form-group">
                    <label for="counts">Products to Show</label>
                    <select name="counts" id="counts" class="form-control form-control-sm">
                        @foreach ($paginatables as $paginatable)
                            <option @if($request->counts && $request->counts == $paginatable) selected @endif  value="{{$paginatable}}">{{$paginatable}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-sm btn-secondary">Filter</button>
            </form>
        </div>
        <div class="col-sm-12 col-md-10">
            <div class=" p-4 bg-white">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>SKU</th>
                        <th>Code</th>
                        <th>Created on</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if($products->count() < 1)
                        <tr>
                            <th colspan="3" class="text-center">
                                No products found
                            </th>
                        </tr>
                    @else 
                        @foreach ($products as $product)
                        <tr>
                            <td><a href="{{route('products.show',[$product])}}">{{$product->SKU}}</a></td>
                            <td>{{$product->code}}</td>
                            <td>{{Carbon::parse($product->created_at)->toFormattedDateString()}}</td>
                            <td>
                                <form action="{{route('products.destroy',$product)}}" class="form-inline" method="post">
                                        @csrf
                                    <button class="btn py-0 btn-link text-danger">Delete</button>
                                    <input type="hidden" name="_method" value="delete">
                                </form>
                            </td>                        
                        @endforeach
                    @endif
                </tbody>
            </table>
            {{$products->appends(['counts' => $request->counts])->links()}}
        </div>
        </div>
    </div>
</div>
<script>
function ProcessForm() {
    var cf = confirm("Are you sure you want to reset the Products and remove all? \n This is not reversable.");
    if(cf){
        return true;
    }
    else{
        event.preventDefault();
    }
}
</script>
@endsection
