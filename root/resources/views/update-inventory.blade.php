@extends('layouts.app')
@section('title',"Update Inventory")
@section('content')

<div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10">
            <h5 class="title">UPDATE INVENTORY {{Carbon::today()->toFormattedDateString()}}</h5>
            </div>
            <div class="col-sm-12 col-md-2 text-right">
                <a href="/updateinventory.csv" class="btn btn-sm btn-danger" download>Download Sample File</a>
            </div>
        </div>
        <hr>  
<div class="row">
    <div class="col-sm-12 col-md-4">
        <form method="POST" action="{{route('inventory.update',0)}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="date">Date of Inventory Update</label>
            <input type="text" readonly name="date" id="date" class="form-control date-picker" value="{{Carbon::today()->toDateString()}}">
            </div>
            <div class="form-group">
                <label for="upload">Upload</label>
                <input type="file" name="upload" id="upload" class="form-control">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>
        </form>
    </div>    
</div>        
</div>
@endsection